#ifndef RAK_CLIENT_H
#define RAK_CLIENT_H

#include "Utility/RakConnection.h"

class RakClient : public RakConnection
{
public:

	RakClient();
	~RakClient();

	virtual void Update()     override;

	//Client specific commands
	void Say(std::string _message, bool _broadcast = true, std::string _recipient = nullptr);
	void SendFile(/*std::string _filePath, bool _broadcast = true, std::string _recipient = nullptr*/);
};

#endif //RAK_CLIENT_H