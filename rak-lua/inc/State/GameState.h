// //
// Game State Class - James Whyte
// Details		- All states derive from this
// Last Edit	- 18/06/2015
// //

#pragma once

#include <ImGui/imgui.h>
#include "State/GameStateManager.h"

class GameState
{
public:

	GameState(unsigned int _id, GameStateManager* gameStateManager);
	~GameState();

	virtual void Initialize();

	virtual void Update(double _deltaTime);
	virtual void Draw();

	unsigned int m_id;

protected:
	virtual void DrawGUI(char* _stateName = "DEBUG STATE");
	GameStateManager* m_gameStateManager;
};