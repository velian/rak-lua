// //
// Custom Physics State Class - James Whyte
// Details		- Made to show off my own physics engine
// Last Edit	- 18/06/2015
// //

#pragma once

#include "GameState.h"

class GameStateManager;
class Camera;
class LuaScript;
class RakClient;
class Client : public GameState
{
public:

	//Constructor, called when added to the gamestatemanager
	Client(unsigned int _id, GameStateManager* _gameStateManager);

	//Initializer, called when switching this to the active state
	virtual void Initialize();

	virtual void Update(double _deltaTime);
	virtual void Draw();

protected:

	virtual void UpdateConsole();
	virtual void DrawGUI(char* _stateName);

private:

	//State objects go in here	
	RakClient* m_client;
	LuaScript* m_luaMain;
	Camera* m_camera;
};