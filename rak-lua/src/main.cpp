#define CATCH_CONFIG_RUNNER
#include "CATCH/catch.hpp"
#include "Application.h"

int main(int argc, char **argv)
{
	//int result = Catch::Session().run(argc, argv);
	Application::Instance()->Initialize(argc, argv);
	Application::Instance()->Run();

	return 0;
}