#include <IMGUI/imgui.h>

#include "State/GameStateManager.h"
#include "State/GameState.h"

//Include all the states we'll be using
#include "State/Client.h"

GameStateManager::GameStateManager()
{

}

GameStateManager::GameStateManager(unsigned int _initialState)
{
	m_currentState = _initialState;
	m_previousState = m_currentState;

	AddState(new Client(0, this));
	//AddState(new Test(0));

	SetState(0);
}

void GameStateManager::AddState(GameState* _gameState)
{
	m_stateList.push_back(_gameState);
}

void GameStateManager::SetState(unsigned int _gameState)
{
	if (m_currentState < m_stateList.size())
	{
		m_previousState = m_currentState;
		m_currentState = _gameState;

		m_stateList[m_currentState]->Initialize();
	}
	else
	{
		printf("Attempted to load NonExistant GameState : %i", _gameState);
	}
}

void GameStateManager::NextState()
{
	if (m_currentState < m_stateList.size() - 1)
	{
		SetState(GetState() + 1);
	}
}

void GameStateManager::PreviousState()
{
	if (m_currentState > 0)
	{
		SetState(GetState() - 1);
	}
}

unsigned int GameStateManager::GetState()
{
	return m_currentState;
}

unsigned int GameStateManager::GetStateSize()
{
	return m_stateList.size();
}

void GameStateManager::Update()
{
	for (unsigned int i = 0; i < m_stateList.size(); i++)
	{
		if (m_stateList[i]->m_id != m_currentState)
		{
			continue;
		}
		else
		{
			m_stateList[i]->Update((double)ImGui::GetIO().DeltaTime);
			return;
		}
	}
}

void GameStateManager::Draw()
{
	for (unsigned int i = 0; i < m_stateList.size(); i++)
	{
		if (m_stateList[i]->m_id != m_currentState)
		{
			continue;
		}
		else
		{
			m_stateList[i]->Draw();
			return;
		}
	}
}