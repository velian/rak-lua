#include "Utility/RakClient.h"
#include "Utility/Lua/LuaScript.h"

RakClient::RakClient() : RakConnection()
{
	LuaScript* luaScript = new LuaScript("./scripts/client.lua");

	m_clientName = luaScript->GetGlobal<std::string>("username");
	m_ipAddress = luaScript->GetGlobal<std::string>("defaultIp");
	m_port = luaScript->GetGlobal<unsigned int>("defaultPort");

	delete luaScript;

	char buffer[16];

	std::string message = "Welcome, ";

	message += m_clientName;
	message += ". Your default port is [";
	
	itoa(m_port, buffer, 10);

	message += buffer;
	message += "]";
	
	ImGui::LogCustomConsole((char*)message.c_str());
}

void RakClient::Update()
{
	if (m_running)
	{
		for (m_packet = m_peer->Receive(); m_packet; m_peer->DeallocatePacket(m_packet), m_packet = m_peer->Receive())
		{
			switch (m_packet->data[0])
			{
			case ID_CLIENT_CONNECT:
			{
				if (m_id == 0)
				{
					BitStream stream(m_packet->data, m_packet->length, false);
					stream.IgnoreBytes(sizeof(RakNet::MessageID));
					stream.Read(m_id);
				}
			} break;
			case ID_CONNECTION_REQUEST_ACCEPTED:
			{
				ImGui::LogCustomConsole("Our connection request has been accepted.\n");
				RakNet::BitStream bsOut;
				bsOut.Write((RakNet::MessageID)ID_CLIENT_CONNECT);
				bsOut.Write((char*)m_clientName.c_str());
				m_peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, m_packet->systemAddress, false);
				m_systemAddress = m_packet->systemAddress;
			} break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
			{
				ImGui::LogCustomConsole("The server is full.\n");
			} break;
			case ID_CLIENT_MESSAGE:
			{
				RakNet::RakString rs;
				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);

				char buffer[256];
				sprintf(buffer, "%s", rs.C_String());

				//std::string bufAsString = buffer;
				//if (bufAsString.find(m_clientName) != std::string::npos)
				//{
				//	break;
				//}
				//else
					ImGui::LogCustomConsole(buffer);
			} break;
			case ID_CLIENT_FTP:
			{
				LuaScript* luaScript;
				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(luaScript);

				//If this worked, then we'll have our script here working and well.
				if (luaScript->m_luaState != nullptr)
				{
					luaScript->RunFile();
				}
				else
				{

				}

			} break;
			}
		}
	}
}

//ToDo : Handle recipient
void RakClient::Say(std::string _message, bool _broadcast, std::string _recipient)
{
	BitStream bs;
	bs.Write((RakNet::MessageID)ID_CLIENT_MESSAGE);

	char buffer[256];
	sprintf(buffer, "%s: %s", m_clientName.c_str(), _message.c_str());

	bs.Write(buffer);
	m_peer->Send(&bs, HIGH_PRIORITY, RELIABLE, 0, UNASSIGNED_RAKNET_GUID, true);
}

void RakClient::SendFile(/*std::string _filePath, bool _broadcast, std::string _recipient*/)
{
	LuaScript* luaScript = new LuaScript("./scripts/main.lua");
	BitStream bs;
	bs.Write((RakNet::MessageID)ID_CLIENT_FTP);

	bs.Write(luaScript);
	m_peer->Send(&bs, HIGH_PRIORITY, RELIABLE, 0, UNASSIGNED_RAKNET_GUID, true);
}