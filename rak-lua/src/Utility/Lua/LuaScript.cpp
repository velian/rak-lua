#include <iostream>
#include <Windows.h>
#include <sys/stat.h>
#include "Utility/Lua/LuaScript.h"
#include "Utility/Lua/LuaLibrary.h"

void report_errors(lua_State *L, int status)
{
	if (status != 0) {
		std::cerr << "-- " << lua_tostring(L, -1) << std::endl;
		lua_pop(L, 1); // remove error message
	}
}

int GetTime(char* _fileName)
{
	long unsigned int time;
	struct _stat buffer;

	if (_stat(_fileName, &buffer))
	{
		time = NULL;
	}
	else
	{
		long unsigned int modifiedTime = (long unsigned int)buffer.st_mtime;
		time = static_cast<int32_t > (modifiedTime);
	}

	return time;
}

LuaScript::LuaScript(char* _fileName)
{
	m_fileName = _fileName;
	m_luaState = luaL_newstate();

	int success = luaL_loadfile(m_luaState, m_fileName);

	if (success == 0)
	{
		m_time = GetTime(m_fileName);

		luaL_openlibs(m_luaState);
		RegisterLibrary(library);
		int s = luaL_dofile(m_luaState, m_fileName);
		report_errors(m_luaState, s);
		m_loaded = true;
	}
	else
	{
		lua_close(m_luaState);
		printf("Failed to load LUA file : %s\n", m_fileName);
		m_loaded = false;
		return;
	}
}

LuaScript::~LuaScript()
{
	if (m_luaState && m_loaded)
	{
		lua_close(m_luaState);
	}
}

void LuaScript::RunFile()
{
	if (m_luaState && m_loaded)
	{
		int newTime = GetTime(m_fileName);

		if (m_time != newTime)
		{
			m_time = newTime;
			luaL_dofile(m_luaState, m_fileName);
		}
	}
}

void LuaScript::RunFunction(std::string _functionName, const char* _format...)
{
	lua_getglobal(m_luaState, _functionName.c_str());

	int argNumber = 0;

	va_list args;
	va_start(args, _format);

	while (*_format != '\0')
	{
		if (*_format == 's')
		{
			char* chr = va_arg(args, char*);
			lua_pushstring(m_luaState, chr);
			argNumber++;
		}
		else if (*_format == 'c')
		{
			char c = va_arg(args, char);
			lua_pushnumber(m_luaState, c);
			argNumber++;
		}
		else if (*_format == 'd')
		{
			int i = va_arg(args, int);
			lua_pushnumber(m_luaState, i);
			argNumber++;
		}
		
		else if (*_format == 'f')
		{
			float flt = va_arg(args, float);
			lua_pushnumber(m_luaState, flt);
			argNumber++;
		}
		_format++;
	}
	va_end(args);
	
	lua_call(m_luaState, argNumber, 0);
}

void LuaScript::Reload()
{
	if (m_luaState && m_loaded)
	{
		luaL_dofile(m_luaState, m_fileName);
	}
}

void LuaScript::RegisterLibrary(const struct luaL_Reg* _library)
{
	if (m_luaState && m_loaded)
	{
		lua_getglobal(m_luaState, "app");
		
		if (lua_isnil(m_luaState, -1))
		{
			lua_pop(m_luaState, 1);
			lua_newtable(m_luaState);
		}		

		luaL_setfuncs(m_luaState, _library, 0);
		lua_setglobal(m_luaState, "app");
	}
}

template<>
int LuaScript::GetGlobal(char* _variableName)
{
	lua_getglobal(m_luaState, _variableName);
	return (int)lua_tonumber(m_luaState, -1);
}

template<>
unsigned int LuaScript::GetGlobal(char* _variableName)
{
	lua_getglobal(m_luaState, _variableName);
	return (unsigned int)lua_tonumber(m_luaState, -1);
}

template<>
bool LuaScript::GetGlobal(char* _variableName)
{
	lua_getglobal(m_luaState, _variableName);
	return (bool)lua_toboolean(m_luaState, -1);
}

template<>
char* LuaScript::GetGlobal(char* _variableName)
{
	lua_getglobal(m_luaState, _variableName);
	return (lua_tostring(m_luaState, -1) != nullptr) ? (char*)lua_tostring(m_luaState, -1) : nullptr;
}

template<>
std::string LuaScript::GetGlobal(char* _variableName)
{
	lua_getglobal(m_luaState, _variableName);
	return (lua_tostring(m_luaState, -1) != nullptr) ? lua_tostring(m_luaState, -1) : nullptr;
}