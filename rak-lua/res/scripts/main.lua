require "scripts.math"

local running = true;

--This is all currently called each frame.
function main()

	--if app.keyDown(string.byte("A")) then
	--	app.log("Pressed A!");
	--end

	--if app.keyDown(string.byte("1")) then
	--	app.clientConnect();
	--end
end


function ClientConnect(name, port, ip)
	app.clientConnect(connection, name, port, ip);
end

function ClientSendFile(filename)
	app.clientSendFile(connection, filename);
end

--[[
function AddRigidbody(type)
	app.log("Hello Wur;d");
	app.addRigidbody(physicsScene, type);
end


function SetTimestep(timestep)
	app.setTimestep(physicsScene, timestep);
end
--]]

local ConsoleCommands = {
	["clientConnect"]  = ClientConnect,
	["clientSendFile"] = ClientSendFile,
	--["Main"] = main,
	--["AddRigidbody"] = AddRigidbody,
	--["SetTimestep"] = SetTimestep,
	--Other functions here
}

function HandleConsoleCommands(command, ...)
	if ConsoleCommands[command] == nil then
		app.log("Invalid console command '" .. command .. "'");
		return;
	end
	ConsoleCommands[command](...);
end