// //
// Application Class - James Whyte
// Details		- Manages the gamestate and basic init functions.
// Last Edit	- 12/06/2015
// //

#pragma once

struct GLFWwindow;
class GameStateManager;
class LuaScript;
class Application
{
public:

	void Run();

	void Initialize(int _argc, char** _argv, unsigned int _width = NULL, unsigned int _height = NULL, char* _windowTitle = nullptr);

	unsigned int GetWidth();
	unsigned int GetHeight();

	void Shutdown();

	static Application* Instance();

private:

	Application();
	~Application();
	Application(Application const&) = delete;

	static Application* m_instance;

	LuaScript* m_configLua;

	GLFWwindow* m_window;
	std::string m_windowTitle;
	unsigned int m_width, m_height;

	GameStateManager* m_gameStateManager;
};