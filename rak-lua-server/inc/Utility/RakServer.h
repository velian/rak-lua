#pragma once

#include "Utility/RakConnection.h"

using namespace RakNet;
class RakServer : public RakConnection
{
public:

	RakServer();
	~RakServer();

	void InitializeServer();
	void InitializeServer(unsigned int _port, char* _ipAddress);

	virtual void Update();

	void SendServerMessage(char* _serverMessage);

private:

	void UpdateServer();

};