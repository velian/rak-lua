#ifndef RAK_CONNECTION_H
#define RAK_CONNECTION_H

#define	DEFAULT_PORT 3180
#define	DEFAULT_IP "127.0.0.1"
#define DEFAULT_NAME "UNDEFINED"
#define MAX_CLIENTS 16

#include <vector>
#include <IMGUI/ImGui.h>
#include <RAKNET/RakPeerInterface.h>
#include <RAKNET/MessageIdentifiers.h>
#include <RAKNET/BitStream.h>
#include <RAKNET/RakNetTypes.h>

using namespace RakNet;
class RakConnection
{
public:

	RakConnection();
	~RakConnection();

	enum ServerMessages
	{
		ID_CLIENT_CONNECT = ID_USER_PACKET_ENUM + 1,
		ID_CLIENT_MESSAGE,
		ID_CLIENT_FTP,
	};

	virtual void Connect(char* _ipAddress = DEFAULT_IP, unsigned int _port = DEFAULT_PORT, char* _clientName = DEFAULT_NAME);
	virtual void Disconnect();

	bool Initialized(){ return m_initialized; }

	virtual void Update() = 0;
	
	unsigned int m_id;
	unsigned int m_port;

	std::string m_clientName;
	std::string m_ipAddress;

	std::vector<char*> m_clientList;

protected:

	bool m_initialized;
	bool m_running;

	RakPeerInterface* m_peer = RakPeerInterface::GetInstance();
	Packet* m_packet;
	SystemAddress m_systemAddress;

	unsigned int m_maxClients;
};

#endif //RAK_CONNECTION_H