#include <GLM/glm.hpp>
#include <GLFW/glfw3.h>
#include <IMGUI/ImGui.h>

#include "Utility/Gizmos.h"
#include "Utility/RakConnection.h"

static int LuaDrawSquareI(lua_State* _luaState)
{
	float positionX = (float)lua_tonumber(_luaState, 1); // get function argument
	float positionY = (float)lua_tonumber(_luaState, 2);
	float positionZ = (float)lua_tonumber(_luaState, 3);
	//float extents = lua_tostring(_luaState, 3); // get function argument
	//glm::vec4 colour = lua_tostring(_luaState, 3); // get function argument
	Gizmos::addAABB(glm::vec3(positionX, positionY, positionZ), glm::vec3(1), glm::vec4(1)); // calling C++ function with this argument...
	return 0;
}

static int LuaDrawSphereI(lua_State* _luaState)
{
	float positionX = (float)lua_tonumber(_luaState, 1); // get function argument
	float positionY = (float)lua_tonumber(_luaState, 2);
	float positionZ = (float)lua_tonumber(_luaState, 3);
	Gizmos::addSphere(glm::vec3(positionX, positionY, positionZ), 1, 16, 16, glm::vec4(1)); // calling C++ function with this argument...
	return 0;
}

static int LuaKeyDownI(lua_State* _luaState)
{
	int key = (int)lua_tonumber(_luaState, 1);
	return glfwGetKey(glfwGetCurrentContext(), (int)key);
}

static int LuaLogI(lua_State* _luaState)
{
	std::string str = lua_tostring(_luaState, 1);
	ImGui::LogCustomConsole((char*)str.c_str());
	return 0;	
}

static int LuaClientConnectI(lua_State* _luaState)
{
	std::string ip;
	std::string str;
	unsigned int port;

	RakConnection* pClient = (RakConnection*)lua_touserdata(_luaState, 1);
	
	if (lua_tostring(_luaState, 2) != nullptr)
	{
		ip = lua_tostring(_luaState, 2);
	}
	else
	{
		ip = pClient->m_ipAddress;
	}

	if (lua_tonumber(_luaState, 3) != 0)
	{
		port = lua_tonumber(_luaState, 3);
	}
	else
	{
		port = pClient->m_port;
	}
	
	if (lua_tostring(_luaState, 4) != nullptr)
	{
		str = lua_tostring(_luaState, 4);
	}	
	else
	{
		str = pClient->m_clientName;
	}

	pClient->Connect((char*)ip.c_str(), port, (char*)str.c_str());

	return 0;
}

static const struct luaL_Reg library[] =
{
	{ "keyDown",		LuaKeyDownI },
	{ "log",			LuaLogI },
	{ "clientConnect",  LuaClientConnectI },
	{ NULL, NULL }  /* sentinel */
};