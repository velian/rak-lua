// //
// Custom Physics State Class - James Whyte
// Details		- Made to show off my own physics engine
// Last Edit	- 18/06/2015
// //

#pragma once

#include "GameState.h"

class GameStateManager;
class LuaScript;
class RakServer;
class Server : public GameState
{
public:

	//Constructor, called when added to the gamestatemanager
	Server(unsigned int _id, GameStateManager* _gameStateManager);

	//Initializer, called when switching this to the active state
	virtual void Initialize();
	virtual void Update(double _deltaTime);

private:

	//State objects go in here	
	RakServer* m_server;
	LuaScript* m_luaConfig;
};