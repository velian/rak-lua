#include <GLFW\glfw3.h>
#include <GL\gl_core_4_4.h>

#include "State/Server.h"

#include "GLM/ext.hpp"

#include "Utility/Lua/LuaScript.h"
#include "Utility/Gizmos.h"

#include "Utility/RakServer.h"

Server::Server(unsigned int _id, GameStateManager* _gameStateManager) : GameState(_id, _gameStateManager)
{
	m_server = new RakServer();

	m_luaConfig = new LuaScript("./scripts/server.lua");

	printf("\nInitializing configuration settings...\n");

	m_server->m_clientName	= m_luaConfig->GetGlobal<std::string>("username");
	m_server->m_ipAddress	= m_luaConfig->GetGlobal<std::string>("defaultIp");
	m_server->m_port		= m_luaConfig->GetGlobal<unsigned int>("defaultPort");

	printf("Configuration settings are all set.\n");

	m_server->InitializeServer();
}

void Server::Initialize()
{

}

void Server::Update(double _deltaTime)
{
	m_server->Update();
}