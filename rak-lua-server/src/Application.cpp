#include <GLFW\glfw3.h>
#include <GL\gl_core_4_4.h>

#include <IMGUI/imgui.h>
#include <IMGUI/imgui_impl_glfw_gl3.h>

#include <string>
#include <stdio.h>
#include <Windows.h>
#include <functional>

#include "Utility/Gizmos.h"
#include "Utility/Lua/LuaScript.h"

#include "Application.h"
#include "State/GameStateManager.h"

Application* Application::m_instance = nullptr;

Application* Application::Instance()
{
	if (m_instance == nullptr)
	{
		m_instance = new Application();
	}
	return m_instance;
}

Application::Application()
{
	m_gameStateManager = nullptr;
}

void Application::Initialize(int _argc, char** _argv, unsigned int _width, unsigned int _height, char* _windowTitle)
{
	m_gameStateManager = new GameStateManager(0);
}

void Application::Run()
{
	while (true)
	{
		m_gameStateManager->Update();
	}
}

unsigned int Application::GetWidth()
{
	return m_width;
}

unsigned int Application::GetHeight()
{
	return m_height;
}

Application::~Application()
{
	
}