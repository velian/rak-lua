#include "Utility/RakConnection.h"

RakConnection::RakConnection()
{
	m_maxClients = MAX_CLIENTS;
	m_port = DEFAULT_PORT;
	m_ipAddress = DEFAULT_IP;
	m_clientName = DEFAULT_NAME;
	m_initialized = false;

	m_running = false;
}

RakConnection::~RakConnection()
{
	Disconnect();
	m_peer->DeallocatePacket(m_packet);
}

void RakConnection::Connect(char* _ipAddress, unsigned int _port, char* _clientName)
{
	ImGui::LogCustomConsole("Initializing...");
	SocketDescriptor sd;
	m_peer->Startup(1, &sd, 1);

	ImGui::LogCustomConsole("Connecting...");
	m_peer->Connect(_ipAddress, (_port == 0) ? m_port : _port, 0, 0);

	if (m_peer->GetConnectionState(m_systemAddress) == RakNet::ConnectionState::IS_CONNECTED)
	{
		ImGui::LogCustomConsole("Connected.");
	}
	else
	{
		ImGui::LogCustomConsole("Could not connect to that IP!");
		m_peer->Shutdown(0);
		return;
	}

	//m_clientName = _clientName;

	m_id = NULL;

	m_initialized = true;
	m_running = true;

	//
}

void RakConnection::Disconnect()
{
	if (!m_initialized || !m_peer) return;
	m_running = false;
}