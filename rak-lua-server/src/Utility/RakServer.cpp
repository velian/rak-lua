#include "Utility/RakServer.h"

RakServer::RakServer()
{

}

void RakServer::InitializeServer()
{
	if (m_port != DEFAULT_PORT && m_ipAddress != DEFAULT_IP)
	{
		InitializeServer(m_port, (char*)m_ipAddress.c_str());
	}
	else if (m_port != DEFAULT_PORT && m_ipAddress == DEFAULT_IP)
	{
		InitializeServer(m_port, DEFAULT_IP);
	}
	else if (m_port == DEFAULT_PORT && m_ipAddress != DEFAULT_IP)
	{
		InitializeServer((unsigned int)DEFAULT_PORT, (char*)m_ipAddress.c_str());
	}
	else
	{
		InitializeServer(DEFAULT_PORT, DEFAULT_IP);
	}
}

void RakServer::InitializeServer(unsigned int _port, char* _ipAddress)
{
	SocketDescriptor sd(_port, 0);
	m_peer->Startup(m_maxClients, &sd, 1);

	m_initialized = true;
	m_running = true;

	printf("Initializing server...\n");
	m_peer->SetMaximumIncomingConnections(m_maxClients);
	printf("Server initialized.\n");
}

//void RakServer::SendServerMessage(char* _message)
//{
	//BitStream bs;
	//bs.Write((RakNet::MessageID)ID_SERVER_BROADCAST);
	//
	//char buffer[256];
	//sprintf(buffer, "SERVER: %s", _message);
	//
	//bs.Write(buffer);
	//m_peer->Send(&bs, HIGH_PRIORITY, RELIABLE, 0, UNASSIGNED_RAKNET_GUID, true);
//}

void RakServer::Update()
{
	if (m_running)
	{
		for (m_packet = m_peer->Receive(); m_packet; m_peer->DeallocatePacket(m_packet), m_packet = m_peer->Receive())
		{
			switch (m_packet->data[0])
			{
			case ID_REMOTE_DISCONNECTION_NOTIFICATION:
			{
				printf("Another client has disconnected.\n");
			} break;
			case ID_REMOTE_CONNECTION_LOST:
			{
				printf("Another client has lost the connection.\n");
			} break;
			case ID_REMOTE_NEW_INCOMING_CONNECTION:
			{
				printf("Another client has connected.\n");
			} break;
			case ID_NEW_INCOMING_CONNECTION:
			{
				printf("A connection is incoming!");
			} break;
			case ID_NO_FREE_INCOMING_CONNECTIONS:
			{
				printf("The server is full.\n");
			} break;
			case ID_DISCONNECTION_NOTIFICATION:
			{
				printf("A client has disconnected.\n");
			} break;
			case ID_CONNECTION_LOST:
			{
				printf("A client lost the connection.\n");
			} break;
			case ID_CLIENT_CONNECT:
			{
				//Server only update --
				RakNet::RakString rs;
				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);

				char* clientName = new char[strlen(rs.C_String())];
				strcpy(clientName, rs.C_String());

				m_clientList.push_back(clientName);

				RakNet::BitStream clientUpdate;
				clientUpdate.Write((RakNet::MessageID)ID_CLIENT_CONNECT);

				clientUpdate.Write(m_clientList.size());
				
				m_peer->Send(&clientUpdate, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_RAKNET_GUID, true);

				char buffer[256];
				sprintf(buffer, "\n%s connected to the server.\n", rs.C_String());

				printf(buffer);
			} break;
			case ID_CLIENT_MESSAGE:
			{
				RakNet::RakString rs;
				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);

				char buffer[256];
				sprintf(buffer, "%s\n", rs.C_String());
				printf(buffer);

				BitStream bs;
				bs.Write((RakNet::MessageID)ID_CLIENT_MESSAGE);
				bs.Write(rs);

				m_peer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_RAKNET_GUID, true);
			} break;
			case ID_CLIENT_FTP:
			{
				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				m_peer->Send(&bsIn, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_RAKNET_GUID, true);
			} break;
			/*case ID_CLIENT_SET_CURRENT_PLAYER:
			{
				unsigned int currentPlayer;

				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(currentPlayer);

				BitStream bsOut;

				bsOut.Write((MessageID)ID_CLIENT_SET_CURRENT_PLAYER);
				bsOut.Write(currentPlayer);

				m_peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_RAKNET_GUID, true);
			} break;*/
			/*case ID_CLIENT_MESSAGE:
			{
				RakNet::RakString rs;
				RakNet::BitStream bsIn(m_packet->data, m_packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);

				char buffer[256];
				sprintf(buffer, "%s\n", rs.C_String());
				ImGui::LogCustomConsole(buffer);

				BitStream bs;
				bs.Write((RakNet::MessageID)ID_SERVER_BROADCAST);
				bs.Write(rs);

				m_peer->Send(&bs, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_RAKNET_GUID, true);
			} break;*/

			//case ID_CLIENT_SEND_MOVE_ORDER:
			//{
			//	//Read in the values from the client, then send them out as a movement order
			//	BitStream bsIn(m_packet->data, m_packet->length, false);
			//	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

			//	glm::vec2 startIndex;
			//	glm::vec2 endIndex;

			//	bsIn.Read(startIndex);
			//	bsIn.Read(endIndex);

			//	//Create our out stream and send the move order to all available clients
			//	BitStream bsOut;

			//	bsOut.Write((RakNet::MessageID)ID_CLIENT_MOVE_PIECE);
			//	bsOut.Write(startIndex);
			//	bsOut.Write(endIndex);

			//	m_peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_RAKNET_GUID, true);

			//} break;

			//case ID_CLIENT_SEND_REMOVE_ORDER:
			//{
			//	//Read in the values from the client, then send them out as a movement order
			//	BitStream bsIn(m_packet->data, m_packet->length, false);
			//	bsIn.IgnoreBytes(sizeof(RakNet::MessageID));

			//	glm::vec2 index;
			//	bsIn.Read(index);

			//	//Create our out stream and send the move order to all available clients
			//	BitStream bsOut;

			//	bsOut.Write((RakNet::MessageID)ID_CLIENT_REMOVE_PIECE);
			//	bsOut.Write(index);

			//	m_peer->Send(&bsOut, HIGH_PRIORITY, RELIABLE_ORDERED, 0, UNASSIGNED_RAKNET_GUID, true);

			//} break;

			default:
			{
				char buffer[256];
				sprintf(buffer, "Message with identifier %i has arrived.\n", m_packet->data[0]);
				printf(buffer);
			} break;
			}
		}
	}
}