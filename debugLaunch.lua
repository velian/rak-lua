require "lfs"

local MAX_CLIENTS = 1;
local MAX_SERVERS = 1;

local CLIENT_PATH = "rak-lua\\bin\\debug\\";
local SERVER_PATH = "rak-lua-server\\bin\\debug\\";

local INITIAL_DIR = lfs.currentdir();

local function InitClients()
	lfs.chdir(CLIENT_PATH);
	for i = 1, MAX_CLIENTS do
		os.execute("start rak-lua.exe");
	end
	lfs.chdir(INITIAL_DIR);
end

local function InitServers()
	lfs.chdir(SERVER_PATH);
	for i = 1, MAX_SERVERS do
		os.execute("start rak-lua-server.exe");
	end
	lfs.chdir(INITIAL_DIR);
end

local function Main()
	print("Initializing clients...");
	InitClients();
	print("Initializing servers...");
	InitServers();
	print("Initialized.");
end

Main();